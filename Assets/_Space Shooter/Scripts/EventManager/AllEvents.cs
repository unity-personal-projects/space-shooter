﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceShooter.Events;

#region GameManager Events

public class GameMenuEvent : SpaceShooter.Events.Event { }
public class GamePlayEvent : SpaceShooter.Events.Event { }
public class GamePauseEvent : SpaceShooter.Events.Event { }
public class GameResumeEvent : SpaceShooter.Events.Event { }
public class GameOverEvent : SpaceShooter.Events.Event { }
public class GameVictoryEvent : SpaceShooter.Events.Event { }

#endregion

#region Gameplay Events

public class LaserTouchEnemyEvent : SpaceShooter.Events.Event
{
    public GameObject laser;
    public GameObject enemy;
}
public class EnemyTouchPlayerEvent : SpaceShooter.Events.Event
{
    public Enemy enemy;
}

#endregion
