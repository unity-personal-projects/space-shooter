﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceShooter.Events;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float m_EnemySpeed = 4f;
    [SerializeField]
    private float m_Strength = 10f;

    public float Strength { get => m_Strength; }

    private void Awake()
    {
        transform.position = new Vector3(Random.Range(-14.5f, 14.5f), 16f, 0);
    }

    void Update()
    {
        transform.Translate(Vector3.down * m_EnemySpeed * Time.deltaTime);

        if (transform.position.y < -16f)
        {
            transform.position = new Vector3(Random.Range(-14.5f, 14.5f), 16f, 0);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<Laser>() != null)
        {
            EventManager.Instance.Raise(new LaserTouchEnemyEvent()
            {
                laser = other.gameObject,
                enemy = gameObject
            });
        }
        if (other.gameObject.GetComponent<Player>() != null)
        {
            EventManager.Instance.Raise(new EnemyTouchPlayerEvent()
            {
                enemy = this
            });
        }
    }

}
