﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceShooter.Events;
using System;

public class EnemyManager : MonoBehaviour
{
    [SerializeField]
    private Vector2 m_SpawnRate = new Vector2(1f, 3f);
    [SerializeField]
    private GameObject m_EnemyPrefab = null;

    private List<GameObject> m_Enemies = new List<GameObject>();

    private void Awake()
    {
        EventManager.Instance.AddListener<LaserTouchEnemyEvent>(OnLaserTouchEnemy);
        EventManager.Instance.AddListener<EnemyTouchPlayerEvent>(OnEnemyTouchPlayer);
    }

    private void OnEnemyTouchPlayer(EnemyTouchPlayerEvent e)
    {
        m_Enemies.Remove(e.enemy.gameObject);
        Destroy(e.enemy.gameObject);
    }

    private void OnLaserTouchEnemy(LaserTouchEnemyEvent e)
    {
        Destroy(e.laser);
        m_Enemies.Remove(e.enemy);
        Destroy(e.enemy);
    }

    private IEnumerator Start()
    {
        while (true)
        {
            m_Enemies.Add(Instantiate(m_EnemyPrefab));
            float cooldown = UnityEngine.Random.Range(m_SpawnRate.x, m_SpawnRate.y);
            yield return new WaitForSeconds(cooldown);
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<LaserTouchEnemyEvent>(OnLaserTouchEnemy);
        EventManager.Instance.RemoveListener<EnemyTouchPlayerEvent>(OnEnemyTouchPlayer);
    }
}
