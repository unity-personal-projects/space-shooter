﻿using UnityEngine;
public class Laser : MonoBehaviour
{
    [SerializeField]
    private float m_BulletSpeed = 5f;
    // [SerializeField]
    // private float m_Damage = 30f;
    [SerializeField]
    private float m_LifeSpan = 4f;

    public float BulletSpeed { get => m_BulletSpeed; }

    private void Awake()
    {
        Destroy(gameObject, m_LifeSpan);
    }
}
