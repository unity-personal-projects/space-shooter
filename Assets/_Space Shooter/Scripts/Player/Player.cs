﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceShooter.Events;
using System;

public class Player : MonoBehaviour
{
    [SerializeField]

    private float m_ScreenSize = 30f;
    [SerializeField]
    private float m_MaxLifePoints = 100f;
    private float m_LifePoints;
    [SerializeField]
    private float m_Speed = 1f;
    [SerializeField]
    private float m_BulletCooldown = 0.2f;

    [SerializeField]
    private Transform m_SpawnPoint = null;

    [SerializeField]
    private GameObject m_LaserPrefab = null;

    private float m_LastFireTime = 0;

    private void Awake()
    {
        EventManager.Instance.AddListener<EnemyTouchPlayerEvent>(OnEnemyTouchPlayer);
        transform.position = Vector3.zero;
        m_LifePoints = m_MaxLifePoints;
    }

    private void OnEnemyTouchPlayer(EnemyTouchPlayerEvent e)
    {
        m_LifePoints -= e.enemy.Strength;
        Debug.Log("Touché : " + m_LifePoints + " / " + m_MaxLifePoints);
    }

    private void Update()
    {
        MovePlayer();
        ShootLaser();
    }

    private void ShootLaser()
    {
        if (Input.GetKey(KeyCode.Mouse0) && Time.time > m_LastFireTime + m_BulletCooldown)
        {
            Vector3 targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPosition.z = 0;
            Vector3 targetDirection = targetPosition - transform.position;
            targetDirection = targetDirection.normalized;

            GameObject g = Instantiate(m_LaserPrefab);
            Laser gl = g.GetComponent<Laser>();
            g.GetComponent<Rigidbody>().velocity = targetDirection * gl.BulletSpeed;
            g.transform.position = m_SpawnPoint.position;
            m_LastFireTime = Time.time;
        }
    }

    private void MovePlayer()
    {
        float yAxis = Input.GetAxis("Vertical");
        float newYPosition = transform.position.y + yAxis * m_Speed * Time.deltaTime;
        newYPosition = Mathf.Clamp(newYPosition, -m_ScreenSize / 2, m_ScreenSize / 2);

        float xAxis = Input.GetAxis("Horizontal");
        float newXPosition = transform.position.x + xAxis * m_Speed * Time.deltaTime;
        newXPosition = Mathf.Clamp(newXPosition, -m_ScreenSize / 2, m_ScreenSize / 2);

        transform.position = new Vector3(newXPosition, newYPosition, 0);
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<EnemyTouchPlayerEvent>(OnEnemyTouchPlayer);
    }
}
